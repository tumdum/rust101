#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;
extern crate rouille;

use std::collections::HashMap;
use std::sync::Mutex;

#[derive(PartialEq, Eq, Hash, Deserialize, Debug)]
struct ID(u32);

#[derive(Debug, Deserialize)]
enum Request {
    Insert{id: ID, val: String},
    Remove(ID),
    Fetch(ID),
    Sum
}

#[derive(Debug, Serialize)]
enum Response {
    Value(String),
    Sum(usize),
    Ok
}

#[derive(Debug)]
enum Error {
    UnknownId,
    AlreadyExists
}

type Result = std::result::Result<Response, Error>;

struct DB {
    storage: HashMap<ID, String>,
}

impl DB {
    fn new() -> Self {
        DB{storage: HashMap::new()}
    }

    fn insert(&mut self, id: ID, val: String) -> Result {
        use std::collections::hash_map::Entry::*;
        match self.storage.entry(id) {
            Occupied(_) => Err(Error::AlreadyExists),
            Vacant(e) => { 
                e.insert(val.into());
                Ok(Response::Ok)
            }
        }
    }

    fn remove(&mut self, id: ID) -> Result {
        self.storage
            .remove(&id)
            .map_or(Err(Error::UnknownId), |_| Ok(Response::Ok))
    }

    fn fetch(&self, id: ID) -> Result {
        self.storage
            .get(&id)
            .cloned()
            .map(Response::Value)
            .ok_or(Error::UnknownId)
    }

    fn sum(&self) -> Result {
        Ok(Response::Sum(
            self.storage
            .values()
            .map(|s| s.len())
            .sum()))
    }

    fn handle(&mut self, req: Request) -> Result {
        match req {
            Request::Insert{id, val} => self.insert(id, val),
            Request::Remove(id) => self.remove(id),
            Request::Fetch(id) => self.fetch(id),
            Request::Sum => self.sum(),
        }
    }
}

#[derive(Debug)]
enum ServiceError {
    Read,
    Deserialization(serde_json::error::Error),
    Db(Error),
    Serialization(serde_json::error::Error),
    UnsupportedMethod(String)
}

impl From<ServiceError> for rouille::Response {
    fn from(e :ServiceError) -> rouille::Response {
        rouille::Response{
            status_code: match e {
                ServiceError::Db(_) => 409,
                ServiceError::Serialization(_) => 500,
                ServiceError::UnsupportedMethod(_) => 405,
                _ => 400
            },
            headers: vec![],
            data: rouille::ResponseBody::from_string(format!("{:?}", e)),
            upgrade: None
        }
    }
}

struct Service(Mutex<DB>);

impl Service {
    fn handle(&self, request: &rouille::Request) -> rouille::Response {
        match request.method() {
            "POST" => request.data()
                        .ok_or(ServiceError::Read)
                        .and_then(|reader| serde_json::from_reader(reader).map_err(ServiceError::Deserialization))
                        .and_then(|req| self.0.lock().unwrap().handle(req).map_err(ServiceError::Db))
                        .and_then(|rsp| serde_json::to_string(&rsp).map_err(ServiceError::Serialization))
                        .map(rouille::Response::text)
                        .unwrap_or_else(|e| e.into()),
            other => ServiceError::UnsupportedMethod(other.into()).into()
        }
    }
}

fn main() {
    let service = Service(Mutex::new(DB::new()));
    rouille::start_server("localhost:8080", move |request| {
        service.handle(request)
    });
}
